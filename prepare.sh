#!/bin/bash

# fetch all submodules
git submodule update --init --recursive

# build libserialport
cd deps/libserialport
./autogen.sh

if [[ "$1" == "linux" ]]; then
    ./configure
elif [[ "$1" == "macos" ]]; then
    ./configure
elif [[ $1 == "windows" ]]; then
    ./configure --build=i686-pc-linux-gnu --host=i686-w64-mingw32
fi

make
sudo make install
cd ../..

# copy libserialport compiled libs
cp deps/libserialport/.libs/* demo/bin

# build Godot API plus serial extension
scons platform=$1

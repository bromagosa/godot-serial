# godot-serial

A Serial Port extension for Godot.

# Building from Linux (Ubuntu/Debian)

## For Linux

Install deps:

```
sudo apt-get install \
  build-essential \
  scons \
  pkg-config \
  libx11-dev \
  libxcursor-dev \
  libxinerama-dev \
  libgl1-mesa-dev \
  libglu1-mesa-dev \
  libasound2-dev \
  libpulse-dev \
  libudev-dev \
  libxi-dev \
  libxrandr-dev
```

Then, run:


```
./prepare.sh linux
./build.sh linux

```

## For MacOS

Install XCode from the App Store.

Install the XCode command-line tools, in a Terminal session:

```
xcode-select --install
```

(Alternatively, if disk space is tight or bandwidth low, it may be possible to skip installing XCode and install only the command-line tools from an installer downloaded from Apple. But this is untested.)

Install Homebrew by following the instructions on the web site.

Use Homebrew to install the required build tools, in a Terminal session:

```
brew install autoconf automake libtool scons
```

Then, run the following from a Terminal session.
The account must have sudo privileges and you must provide your password when asked.

```
./prepare.sh macos
./build.sh macos
```

In order to use this extension in a Godot project, do the following:

```
cd demo/bin
# Fix links to /usr/local:
install_name_tool -change /usr/local/lib/libserialport.0.dylib bin/libserialport.0.dylib libgdserial.macos.template_debug.framework/libgdserial.macos.template_debug
# Check your work; you should not see /usr/local in the output
# and you should see bin/libserialport.0.dylib
otool -L libgdserial.macos.template_debug.framework/libgdserial.macos.template_debug
```
and similarly for `libgdserial.macos.template_release.framework`, if using that.

Then, if you plan to distribute your Godot projects to others, delete the files installed in /usr/local, so you can be sure that the Godot extension is not relying on them:
```
sudo rm /usr/local/lib/libserialport.*
sudo rm /usr/local/include/libserialport.h
```

Finally, copy each of these files from `demo/bin` to the `bin` directory of each of your Godot projects:

- libgdserial.macos.template_debug.framework/libgdserial.macos.template_debug
- libserialport.0.dylib
- macosx.o

## For Windows

Install deps:

```
sudo apt-get install mingw-w64
```

Then, run:

```
./prepare.sh windows
./build.sh windows

```

If that fails, you probably need to reconfigure mingw to use POSIX. Here's how
to do it:

```
sudo update-alternatives --config i686-w64-mingw32-gcc
sudo update-alternatives --config i686-w64-mingw32-g++
sudo update-alternatives --config x86_64-w64-mingw32-gcc
sudo update-alternatives --config x86_64-w64-mingw32-g++
```

In each of these cases, select the option with "POSIX" in its name.

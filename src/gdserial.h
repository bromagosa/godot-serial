#ifndef GDSERIAL_H
#define GDSERIAL_H

#include <godot_cpp/classes/node.hpp>
#include <libserialport.h>

namespace godot {

class SerialPort : public Node {
	GDCLASS(SerialPort, Node)

private:
	godot::String port_name;
	int baud_rate;

protected:
	static void _bind_methods();
	int check(enum sp_return code);
	struct sp_port *port;

public:
	SerialPort();
	~SerialPort();

	void _process(double delta) override;

	// API
	void set_port_name(godot::String name);
	godot::String get_port_name();
	godot::Array get_port_list();
	void open();
	void close();
	int send_bytes(godot::PackedByteArray bytes);
	godot::PackedByteArray receive_bytes();
};

}

#endif

#include "gdserial.h"
#include <godot_cpp/core/class_db.hpp>

using namespace godot;

// Class definition

void SerialPort::_bind_methods() {
	// methods
	godot::ClassDB::bind_method(godot::D_METHOD("set_port_name", "name"), &SerialPort::set_port_name);
	godot::ClassDB::bind_method(godot::D_METHOD("get_port_name"), &SerialPort::get_port_name);
	godot::ClassDB::bind_method(godot::D_METHOD("get_port_list"), &SerialPort::get_port_list);
	godot::ClassDB::bind_method(godot::D_METHOD("open"), &SerialPort::open);
	godot::ClassDB::bind_method(godot::D_METHOD("close"), &SerialPort::close);
	godot::ClassDB::bind_method(godot::D_METHOD("send_bytes", "bytes"), &SerialPort::send_bytes);
	godot::ClassDB::bind_method(godot::D_METHOD("receive_bytes"), &SerialPort::receive_bytes);

	// properties
	ClassDB::add_property("SerialPort", PropertyInfo(Variant::STRING, "port_name"), "set_port_name", "get_port_name");

	// signals
	ADD_SIGNAL(MethodInfo("port_connected"));
	ADD_SIGNAL(MethodInfo("port_disconnected"));
	ADD_SIGNAL(MethodInfo("serial_port_error", PropertyInfo(Variant::STRING, "error_message")));
}

// Initialization and cleanup

SerialPort::SerialPort() {
	baud_rate = 115200;
	struct sp_port *port;
}

SerialPort::~SerialPort() {
	// Add your cleanup here.
}

// Godot overridables

void SerialPort::_process(double delta) {
}

// Class Methods

// Internal stuff

int SerialPort::check(enum sp_return result) {
	char *error_message;
	switch (result) {
		case SP_ERR_ARG:
			emit_signal("serial_port_error", "Invalid argument.");
		case SP_ERR_FAIL:
			error_message = sp_last_error_message();
			emit_signal("serial_port_error", error_message);
			sp_free_error_message(error_message);
		case SP_ERR_SUPP:
			emit_signal("serial_port_error", "Not supported.");
		case SP_ERR_MEM:
			emit_signal("serial_port_error", "Couldn't allocate memory.");
		case SP_OK:
		default:
			return result;
	}	
}

// Godot API

void SerialPort::set_port_name(godot::String name) {
	port_name = name;
}

godot::String SerialPort::get_port_name() {
	return port_name;
}

godot::Array SerialPort::get_port_list() {
	struct sp_port **ports;
	sp_return result = sp_list_ports(&ports);

	if (result != SP_OK) {
		emit_signal("serial_port_error", "Could not get port list");
		sp_free_port_list(ports);
		return {};
	}

	godot::TypedArray<String> port_names = {};
	for (sp_port **ptr = ports; *ptr; ++ptr) {
		const char *port_name = sp_get_port_name(*ptr);
		godot::String s = port_name;
		port_names.push_back(s);
	}

	sp_free_port_list(ports);
	return port_names;
}

void SerialPort::open() {
	int fail;
	fail = check(sp_get_port_by_name(port_name.utf8().get_data(), &port));
	fail |= check(sp_open(port, SP_MODE_READ_WRITE));
	fail |= check(sp_set_baudrate(port, baud_rate));
	fail |= check(sp_set_bits(port, 8));
	fail |= check(sp_set_parity(port, SP_PARITY_NONE));
	fail |= check(sp_set_stopbits(port, 1));
	fail |= check(sp_set_flowcontrol(port, SP_FLOWCONTROL_NONE));
	if (!fail) {
		emit_signal("port_connected");
	}
}

void SerialPort::close() {
	check(sp_close(port));
	sp_free_port(port);
}

int SerialPort::send_bytes(godot::PackedByteArray bytes) {
	int result;
	result =
		check(
			sp_blocking_write(
				port,			// port pointer
				bytes.ptr(),	// bytes
				bytes.size(),	// size
				100 			// timeout (ms)
			)
		);
	return 0;
}

godot::PackedByteArray SerialPort::receive_bytes() {
	char buf[512];
	godot::PackedByteArray data;
	int result = check(
		sp_blocking_read(
			port,	// port pointer
			buf,	// receive buffer
			512,	// receive size
			100 	// timeout
		)
	);
	if (result > 0) {
		data.resize(result);
		memcpy(data.begin().operator->(), buf, data.size());
	}
	return data;
}


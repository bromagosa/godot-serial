#ifndef GDSERIAL_REGISTER_TYPES_H
#define GDSERIAL_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_serial_module(ModuleInitializationLevel p_level);
void uninitialize_serial_module(ModuleInitializationLevel p_level);

#endif // GDSERIAL_REGISTER_TYPES_H

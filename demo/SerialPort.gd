extends SerialPort

var connected = false
# Called when the node enters the scene tree for the first time.
func _ready():
	print(get_port_list())
	set_port_name('/dev/ttyUSB0')
	open()

func _process(delta):
	if (connected):
		print(receive_bytes())

func _on_port_connected():
	connected = true
	print('connected')
